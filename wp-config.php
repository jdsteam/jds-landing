<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jds_landing');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hw},vT%`~@|FsLMEgm/$[8W0qQd5J(CJPZPA;e<pff;J`k@7>b{sVnLK~F%P]u,8');
define('SECURE_AUTH_KEY',  '1ZC4XD|R/@GRkY$0 Eb7za.;vQ5=[r@v_50 _|2nB8@U*&lh){YtdDH)|NS;lJ@!');
define('LOGGED_IN_KEY',    'OkoTg9%>?QJZ{eZecc(@A3I2bSWk*)cj%eJhus+Jv`[_zce)Ze1Qb 7]WOman+u%');
define('NONCE_KEY',        '<uf~hTVUsXSsc,CiO$S~ie/t;1r~}U);krivr+^qk<4X-GL=<)C6CKC<qD>%IVKR');
define('AUTH_SALT',        '#Hy~#5 *r>n|!9*`D,9c;F8aV.)pwh5[(0AT|] Mqw0x:16`^os/Fz_4?DhHZds>');
define('SECURE_AUTH_SALT', '$d=q<Y3?he7NLHnriQJOlXI2ma-B+p46!2}2ly@;H KRIA.X9ofdwF!Xk|Nf_.{1');
define('LOGGED_IN_SALT',   '4lRx%h?ARd<u^w^IY}[4;j94Vxq=l`MTo=+tam,3%MMNsf[^WIa b-##@U1&R#*/');
define('NONCE_SALT',       'u8HB@}+fs~0$bzm)t8Xq(Y&ms^ZpVS?4:6Ky^2?&-_gce^X*gA9Q}zag@J>V+G0>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
